import java.net.*;
import java.io.*;

public class opendoor {

    static boolean debug = true;

    public static void main(String args[]) throws Exception {
      System.out.println("* try to open tronlab's moleflap");
      File keyfile = new File(".moleflap.c3d2");
      if(!keyfile.exists()) {
        keyfile.createNewFile();
        System.out.println("please fill "+keyfile.getAbsolutePath()+" with your token.");
        System.exit(1);
      }
      FileInputStream fistream = new FileInputStream(keyfile.getAbsolutePath());
      DataInputStream in = new DataInputStream(fistream);
      BufferedReader reader = new BufferedReader(new InputStreamReader(in));
      String line = reader.readLine();
      if(line == null) line = "";
      if(line.length() != 164) {
        System.out.println("token should be valid (164 chars). not like this: '"+line+"'");
        in.close();
        System.exit(1);
      }
      if(debug) System.out.println("old: "+line);
      in.close();
      fistream.close();

      URL url = new URL("http://moleflap.hq.c3d2.de/open?"+line);
      InputStream uistream = url.openStream();
      BufferedInputStream bufIn = new BufferedInputStream(uistream);
      line = ""; int data;
      for (;;) {
        data = bufIn.read();
        if (data == 10 || data == -1) break;
          else line += (char) data;
       }
       if(debug) System.out.println("new: "+line);
       bufIn.close();
       uistream.close();

       System.out.println("* check door ...");
       FileWriter fostream = new FileWriter(keyfile.getAbsolutePath());
       BufferedWriter out = new BufferedWriter(fostream);
       out.write(line);
       out.close();
       fostream.close();

       if(debug) System.out.println("done.");
  }
}
