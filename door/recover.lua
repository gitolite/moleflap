#!/usr/bin/env lua

require 'luasql.postgres'
require 'md5'

require 'helper'
require 'token'
require 'config'

function fail(msg)
    print "Status: 423 Locked"
    print ""
    print("Error: " .. msg)
end

print "Content-Type: text/plain"

prefix = os.getenv("QUERY_STRING")

env = luasql.postgres()
con = env:connect(config.db)

if prefix == nil or prefix == "" then
  fail("No token given")
else
  if prefix:sub(1, 2) == "t=" then
    prefix = prefix:sub(3)
  end

  if prefix:len() == config.prefix_len then
    if not is_bruteforce(con) then
      msg, why = encrypt_key(prefix)
      if not msg then
        fail(why)
      end
      print("")
      print(msg)
    else
      fail("Stop bruteforcing, this box is too slow")
    end
  else
    fail("Invalid token")
  end
end

con:close()
env:close()
