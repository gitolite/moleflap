#!/usr/bin/env lua

require 'luasql.postgres'
require 'helper'
require 'config'
require 'base64'

function exit(env, con)
  con:close()
  env:close()
  os.exit()
end

print "* checking token if it's dead ..."

env = luasql.postgres()
con = env:connect(config.db)

io.stdout:write("enter prefix (length "..config.prefix_len.."): ")
prefix = io.stdin:read()

if prefix:len() ~= config.prefix_len then
  print("ERROR - prefix must have given length "..config.prefix_len..".")
  exit(env,con)
end
if not is_base64(prefix) then
  print "ERROR - prefix must be a valid base64 string."
  exit(env,con)
end

deadtokencount = con:execute("select count(*) from graveyard where prefix='"..prefix.."';"):fetch()
if deadtokencount == nil then
  print "ERROR - can't get correct results from database. PLease check!"
  exit(env,con)
end

deadtokencount = tonumber(deadtokencount)
if deadtokencount == 0 then
    print "no token found in graveyard."
  elseif deadtokencount == 1 then
    print "token found. token is dead."
  else
    print "ERROR - found more than 1 dead token .. something is broken. Please do something!"
end

exit(env,con)

