#!/usr/bin/env lua

require 'luasql.postgres'
require 'md5'

require 'helper'
require 'token'
require 'config'

function fail(msg)
    print "Status: 423 Locked"
    print ""
    print("Error: " .. msg)
end

print "Content-Type: text/plain"

token = os.getenv("QUERY_STRING")

env = luasql.postgres()
con = env:connect(config.db)

if token == nil or token == "" then
  fail("No token given")
else
  if token:sub(1, 2) == "t=" then
    token = token:sub(3)
  end

  token = token:gsub(" ",""):gsub("\n","")

  if token:len() == config.key_len then
    if not is_bruteforce(con) then
      print ""
      b = check_token(con, token)
      token = set_prefix(token,generate_token())
      if b ~= false then
        add_token(con, token)
        qspawn("./openseamless")
      end
      print(token)
    else
      fail("Stop bruteforcing, this box is too slow")
    end
  else
    fail("Invalid token")
  end
end

con:close()
env:close()
