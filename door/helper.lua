
function kill_stdio()
  io.stdout:close()
  io.stderr:close()
  io.stdin:close()
end

function qspawn(program) -- quit  spawn
  kill_stdio()
  os.execute(program .. " &> /dev/null &")
end

function rows(con, stmt)
  local cur = assert (con:execute(stmt))
  return function ()
    return cur:fetch()
  end
end

function is_bruteforce(con)
  if not os.getenv('REMOTE_ADDR') then
     return false
  end
  now = os.time()
  host = md5.sumhexa(os.getenv('REMOTE_ADDR'))
  clean = "delete from lock where death<" .. now .. ";"
  check = "select count(*) from lock where host='" .. host .. "';"
  rn = tonumber(con:execute(clean .. check):fetch())

  if rn < config.lock then
    con:execute("insert into lock (death, host) values(" .. (now + config.lock_time) .. ", '".. host .."');")
    return false
  else
    return true
  end
end

