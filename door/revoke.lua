#!/usr/bin/env lua

require 'luasql.postgres'
require 'helper'
require 'config'
require 'base64'

function exit(env, con)
  con:close()
  env:close()
  os.exit()
end

print "* revoking token ..."

env = luasql.postgres()
con = env:connect(config.db)

io.stdout:write("enter prefix (length "..config.prefix_len.."): ")
prefix = io.stdin:read()

if prefix:len() ~= config.prefix_len then
  print("ERROR - prefix must have given length "..config.prefix_len..".")
  exit(env,con)
end
if not is_base64(prefix) then
  print "ERROR - prefix must be a valid base64 string."
  exit(env,con)
end

if 0 == con:execute("select ttl from tokens where prefix='"..prefix.."';"):numrows() then
  print("ERROR prefix '"..prefix.."' not found in database.")
  exit(env,con)
end

io.stdout:write("are you sure you want to delete all tokens with prefix '" .. prefix .. "': [yes|no] ")
answer = io.stdin:read()
if answer == "yes" then
  assert(con:execute("delete from tokens where prefix='"..prefix.."'; delete from graveyard where prefix='"..prefix.."';"))
  print "* token removed"
  exit(env,con)
end

print "* action canceled"

exit(env,con)

