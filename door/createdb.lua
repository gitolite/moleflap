#!/usr/bin/env lua

require 'luasql.postgres'
require 'config'

print("* creating db ...")

pl  = config.prefix_len
len = config.key_len
env = luasql.postgres()
con = env:connect(config.db)

assert(con:execute("create table tokens ( prefix char("..pl..") primary key, token char("..len..") unique not null, ttl int, gpg_id char(15) null);")) -- ttl - time to live
assert(con:execute("create table graveyard ( prefix char("..pl..") primary key, token char("..len..") unique not null, ttrd int, gpg_id char(15) null);")) -- ttrd - time to real death
assert(con:execute("create table users ( name text primary key, ntc int);")) -- ntc - new token count
assert(con:execute("create table lock (death int, host text);"))

con:close()
env:close()
