#!/usr/bin/env lua

require 'luasql.postgres'
require 'helper'
require 'config'
require 'token'
require 'base64'

function exit(env, con)
  con:close()
  env:close()
  os.exit()
end



print "set gpg token ..."

env = luasql.postgres()
con = env:connect(config.db)

io.stdout:write("enter prefix: ")
prefix = io.stdin:read()

if prefix == "" then
    print "* action canceled"
else
    local pgp_id = con:execute("select gpg_id from tokens where prefix='"..prefix.."';"):fetch() 
    if pgp_id then
        print("current gpg_id: "..pgp_id)
    else
        print("current gpg_id: none")
    end
    edit_pgp(prefix)
end


exit(env,con)

