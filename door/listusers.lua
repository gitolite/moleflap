#!/usr/bin/env lua

require 'luasql.postgres'
require 'helper'
require 'config'

env = luasql.postgres()
con = env:connect(config.db)

i = 0
print "* current users:"
print "-ntc-|-username----------------"
for name, ntc in rows(con,"select * from users;") do print(string.format("%5s| %s",ntc,name)) i=i+1 end
print(i .. " users.")
print "(ntc - new token count)"

con:close()
env:close()
