#!/usr/bin/env lua

require 'luasql.postgres'
require 'helper'
require 'config'

env = luasql.postgres()
con = env:connect(config.db)

print "* statistic:"

print("valid tokens: "..con:execute("select count(*) from tokens;"):fetch())
print("dead tokens: "..con:execute("select count(*) from graveyard;"):fetch())
print("users: "..con:execute("select count(*) from users;"):fetch())

con:close()
env:close()
