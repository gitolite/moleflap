#!/usr/bin/env lua

require "luasql.postgres"
require "config"

print "* reset db ..."
io.stdout:write("do you want to delete all tokens? [yes|no] ")
first = io.stdin:read()
if first == "yes" then
  io.stdout:write("how should everybody feel about you? [hate|love] ")
  second = io.stdin:read()
  if second == "hate" then
    io.stdout:write("really? [sure|no] ")
    third = io.stdin:read()
    if third == "sure" then
      
        env = luasql.postgres()
        con = env:connect(config.db)
        t = "drop table if exists graveyard;" ..
            "drop table if exists tokens;" ..
            "drop table if exists users;" ..
            "drop table if exists lock;"
        assert(con:execute(t))
        con:close()
        env:close()

        print "everybody hates you now 'cause all tokens are gone."

      elseif third == "no" then
        print "well done young padawan."
    end
  end
end
