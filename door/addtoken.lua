#!/usr/bin/env lua

require 'luasql.postgres'
require 'helper'
require 'config'
require 'token'
require 'base64'

function exit(env, con)
  con:close()
  env:close()
  os.exit()
end

print "* adding token ..."

env = luasql.postgres()
con = env:connect(config.db)

print "current users:"
for name in rows(con,"select name from users;") do print(name) end

io.stdout:write("enter name: ")
name = io.stdin:read()

if name == "" then
    print "* action canceled"
  else
    token = generate_token()
    print "* generate token"

    io.stdout:write("enter prefix (length "..config.prefix_len.." chars): [default random] ")
    prefix = io.stdin:read()
    if prefix == "" then
        prefix = token:sub(1,config.prefix_len)
      else
        if prefix:len() ~= config.prefix_len then
          print("ERROR - prefix must have given length "..config.prefix_len..".")
          exit(env,con)
        end
        if not is_base64(prefix) then
          print "ERROR - prefix must be a valid base64 string."
          exit(env,con)
        end
        if 0 ~= con:execute("select ttl from tokens where prefix='"..prefix.."';"):numrows() then
          print "ERROR - prefix already in use."
          exit(env,con)
        end
    end
    token = set_prefix(prefix,token)

    while true do
        io.stdout:write("enter gpg id: [] ")
        gpg_id = io.stdin:read()
        if prefix == "" then
            break
        end
        if os.execute("gpg --batch --recv-keys "..gpg_id) == 0 then
            break
        end
    end


    if 0 ~= con:execute("select ttl from tokens where token='"..token.."';"):numrows() then
      print(token)
      print "ERROR - token already exists .. please try again."
      exit(env,con)
    end

    cur = con:execute("insert into users (name, ntc) values ('"..name.."', 1);")
    if cur == 1 then
        print("* add user " .. name)
      else
        assert(con:execute("update users set ntc=ntc+1 where name='"..name.."';"))
    end
    ttl = os.time() + config.ttl
    assert(con:execute("insert into tokens (prefix, token, ttl) values ('"..prefix.."', '"..token.."', "..ttl..");"))
    assert(con:execute("delete from graveyard where prefix='"..prefix.."';"))
    print "* add token:"
    print(token)
    print("* prefix to remember: "..prefix)
        while true do
        io.stdout:write("enter gpg id: [] ")
        gpg_id = io.stdin:read()
        if prefix == "" then
            break
        end
        if os.execute("gpg --batch --recv-keys "..gpg_id) == 0 then
            break
        end
    end
end

exit(env,con)

