#!/usr/bin/env lua

config = {
  ['db'] = "cricket",
  ['prefix_len'] = 4,
  ['check_len'] = 8,                -- size of bytes to use as unique identifier
  ['open_cmd'] = "ssh root@fe80::218:84ff:fe1d:3fbc%eth0 door &",
  ['key_len'] = 164,				-- key_len/8*6 must be an integer!!!1!
  ['ttl'] = 60 * 60 * 24 * 7 * 8,	-- s m h d w factor Time To Life (while alive)
  ['ttrd'] = 60 * 60 * 24 * 365,	-- s m h d y Time To Real Death (while in graveyard)
  ['ruttl'] = 60 * 60 * 24 * 7,		-- s m h d w Random time added to Time To Life (after used) (will be randomized)
  ['lock'] = 20,					-- allowed requests per time
  ['lock_time'] = 60,				-- secs per allowed requests
}

