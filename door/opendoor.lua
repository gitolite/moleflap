#!/usr/bin/env lua

require 'config'
require 'helper'

function open_door()
  kill_stdio()
  os.execute(config.open_cmd)
end

open_door()

